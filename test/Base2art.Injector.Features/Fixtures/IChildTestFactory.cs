﻿namespace Base2art.ComponentModel.Fixtures
{
    public interface IChildTestFactory
    {
        string GetInstanceName();
    }
}
