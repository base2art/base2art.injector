﻿namespace Base2art.ComponentModel.Fixtures
{
    public class ChildTestFactory : IChildTestFactory
    {
        private readonly ITestFactory testFactory;

        public ChildTestFactory(ITestFactory testFactory)
        {
            this.testFactory = testFactory;
        }

        public string GetInstanceName()
        {
            return this.testFactory.GetInstanceNumber().ToString();
        }
    }
}