﻿namespace Base2art.ComponentModel.Fixtures
{
	public interface ITestFactoryProxy
	{
		int GetInstanceNumber();
	}
}

