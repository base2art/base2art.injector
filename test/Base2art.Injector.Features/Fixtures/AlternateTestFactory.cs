﻿namespace Base2art.ComponentModel.Fixtures
{
    public class AlternateTestFactory : ITestFactory
    {
        public int GetInstanceNumber()
        {
            return 42;
        }
    }
}