﻿using System;
namespace Base2art.ComponentModel.Fixtures.AsyncExample
{
    public class PersonService
    {
        public PersonService(IUserService userService, ICacheService cache)
        {
        }
    }
    public class PersonServiceWithSlowDown
    {
        public static int i;
        public PersonServiceWithSlowDown(IUserService userService, ICacheService cache)
        {
            i += 1;
            var start = DateTime.Now;
            while (DateTime.Now < start.AddMilliseconds(1000))
            {
//                Console.WriteLine("Waiting on " + i);
            }
        }
    }
    
    public interface ICacheService
    {
        
    }
    
    public interface IUserService
    {
        
    }
    
    public class UserService : IUserService
    {
        public UserService(ILogger logger, ICacheService service)
        {
        }
    }
    
    public class CacheService : ICacheService
    {
        public CacheService(ILogger logger)
        {
        }
    }
    public interface ILogger
    {
        
    }
    
    public class Logger : ILogger
    {
        
    }
}
