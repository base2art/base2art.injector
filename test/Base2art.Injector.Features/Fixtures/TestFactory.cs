﻿namespace Base2art.ComponentModel.Fixtures
{
    public class TestFactory : ITestFactory
    {
        private static int counter;

        private readonly int instanceNumber = -1;

        public TestFactory()
        {
            counter += 1;
        }

        public TestFactory(int i)
        {
            this.instanceNumber = i;
            counter += 1;
        }

        public int GetInstanceNumber()
        {
            return this.instanceNumber != -1 ? this.instanceNumber : counter;
        }
    }
}