﻿namespace Base2art.ComponentModel.Fixtures
{
    public class ChildTestFactoryMultiple : IChildTestFactory
    {
        private readonly ITestFactory[] testFactories;

        public ChildTestFactoryMultiple(ITestFactory[] testFactories)
        {
            this.testFactories = testFactories;
        }

        public string GetInstanceName()
        {
            string combined = string.Empty;
            foreach (var factory in this.testFactories)
            {
                combined += factory.GetInstanceNumber().ToString();
            }

            return combined;
        }
    }
}