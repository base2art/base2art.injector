﻿namespace Base2art.ComponentModel.Fixtures
{
	public class TestFactoryProxy : ITestFactoryProxy
	{
		ITestFactory inner;

		public TestFactoryProxy(ITestFactory inner)
		{
			this.inner = inner;
		}

		public int GetInstanceNumber()
		{
			return this.inner == null ? -85 : this.inner.GetInstanceNumber();
		}
	}
}

