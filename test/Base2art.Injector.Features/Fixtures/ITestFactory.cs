﻿namespace Base2art.ComponentModel.Fixtures
{
    public interface ITestFactory
    {
        int GetInstanceNumber();
    }
}