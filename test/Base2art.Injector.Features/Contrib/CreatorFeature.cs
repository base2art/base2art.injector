﻿namespace Base2art.ComponentModel.Contrib
{
    using System;
    using System.Collections.Generic;
    using Base2art.ComponentModel;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class CreatorFeature
    {
        private readonly IDictionary<string, object> parms = new Dictionary<string, object>();
        private readonly IDictionary<string, object> props = new Dictionary<string, object>();
        
        [SetUp]
        public void BeforeEach()
        {
            parms.Clear();
            props.Clear();
        }
        
        [Test]
        public void ShouldResolve()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            parms["name"] = "MyName";
            var parms1 = parms.Copy();
            resolver.Bind<Service1>()
                .To(x => x.CreateFrom<Service1>(parms1, props.Copy()));
            
            parms.Clear();
            parms["name"] = "OtherName";
            resolver.Bind<Service2>()
                .To(x => x.CreateFrom<Service2>(parms, props.Copy()));
            
            Service1 s = (Service1)resolver.Resolve(typeof(Service1));
            Assert.AreEqual(s.Inner.Name, "OtherName");
            Assert.AreEqual(s.Name, "MyName");
            resolver.Resolve(typeof(Service1));
        }
        
        [Test]
        public void ShouldResolveInterface()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            parms["name"] = "MyName";
            var parms1 = parms.Copy();
            resolver.Bind<IService1>()
                .To(x => x.CreateFrom<Service1>(parms1, props.Copy()));
            
            parms.Clear();
            parms["name"] = "OtherName";
            resolver.Bind<Service2>()
                .To(x => x.CreateFrom<Service2>(parms, props.Copy()));
            
            IService1 s = (IService1)resolver.Resolve(typeof(IService1));
            Assert.AreEqual(s.Inner.Name, "OtherName");
            Assert.AreEqual(s.Name, "MyName");
            resolver.Resolve(typeof(IService1));
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Success_Int()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = "234234";
            resolver.Bind<MissingFieldVerifier<int>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<int>>(parms, props));
            
            Func<MissingFieldVerifier<int>> a = () => resolver.Resolve<MissingFieldVerifier<int>>();
            a().Data.Should().Be(234234);
            
        }
        
        [Test]
        public void ShouldNotResolveMissingField_MalFormed_Int()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = "234234dfsdf";
            resolver.Bind<MissingFieldVerifier<int>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<int>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<int>>();
            var inner = a.ShouldThrow<Exception>().And.InnerException;
            inner.Should().BeOfType(typeof(FormatException));
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Missing_Int()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<int>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<int>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<int>>();
            a.ShouldThrow<MissingFieldException>();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Success_NullableInt()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = "234234";
            resolver.Bind<MissingFieldVerifier<int?>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<int?>>(parms, props));
            
            Func<MissingFieldVerifier<int?>> a = () => resolver.Resolve<MissingFieldVerifier<int?>>();
            a().Data.Should().Be(234234);
            
        }
        
        [Test]
        public void ShouldNotResolveMissingField_MalFormed_NullableInt()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = "234234dfsdf";
            resolver.Bind<MissingFieldVerifier<int?>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<int?>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<int?>>();
            var inner = a.ShouldThrow<Exception>().And.InnerException;
            inner.Should().BeOfType(typeof(FormatException));
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Missing_NullableInt()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<int?>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<int?>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<int?>>();
            a.ShouldThrow<MissingFieldException>();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Success_String()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = "234234";
            resolver.Bind<MissingFieldVerifier<string>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<string>>(parms, props));
            
            Func<MissingFieldVerifier<string>> a = () => resolver.Resolve<MissingFieldVerifier<string>>();
            a().Data.Should().Be("234234");
        }
        
        [Test]
        public void ShouldResolveWithInstance_WithInterfaces()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.RegisterInstance(new MissingPropVerifier<string>{ Data = "asd" }, true);
            
            Func<MissingPropVerifier<string>> a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a().Data.Should().Be("asd");
            
            Func<IMissingPropVerifier<string>> b = () => resolver.Resolve<IMissingPropVerifier<string>>();
            b().Data.Should().Be("asd");
        }
        
        [Test]
        public void ShouldResolveWithInstance_WithNoInterfaces()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.RegisterInstance(new MissingPropVerifier<string>{ Data = "asd" }, false);
            
            Func<MissingPropVerifier<string>> a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a().Data.Should().Be("asd");
            
            Func<IMissingPropVerifier<string>> b = () => resolver.Resolve<IMissingPropVerifier<string>>(true);
            b().Should().BeNull();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_MalFormed_String()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = new NonStringable{ };
            resolver.Bind<MissingFieldVerifier<string>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<string>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<string>>();
            var inner = a.ShouldThrow<Exception>();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Missing_String()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<string>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<string>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<string>>();
            a.ShouldThrow<MissingFieldException>();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Success_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = new NonStringable();
            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(parms, props));
            
            Func<MissingFieldVerifier<NonStringable>> a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            a().Data.Should().Be(parms["data"]);
        }
        
        [Test]
        public void ShouldNotResolveMissingField_MalFormed_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = "sdf";
            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            var inner = a.ShouldThrow<Exception>();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_Missing_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            a.ShouldThrow<KeyNotFoundException>();
        }
        
        [Test]
        public void ShouldNotResolveMissingField_IsNull_NonStringable()
        {
            var resolver = ServiceLoader.CreateLoader();

            parms["data"] = null;
            resolver.Bind<MissingFieldVerifier<NonStringable>>()
                .To(x => x.CreateFrom<MissingFieldVerifier<NonStringable>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingFieldVerifier<NonStringable>>();
            a.ShouldNotThrow();
        }
        
        [Test]
        public void ShouldCreateAnonymousObject_Singleton()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            var testObject = new {dsd = 10};
            props["dsd"] = "234";
            resolver.Register(testObject.GetType(), testObject.GetType(), props, null, true);
            
            dynamic obj = resolver.Resolve(testObject.GetType());
            int rez = obj.dsd;
            rez.Should().Be(234);
        }
        
        [Test]
        public void ShouldCreateAnonymousObject_Instance()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            var testObject = new {dsd = 10};
            props["dsd"] = "234";
            resolver.Register(testObject.GetType(), testObject.GetType(), props, null, false);
            
            dynamic obj = resolver.Resolve(testObject.GetType());
            int rez = obj.dsd;
            rez.Should().Be(234);
        }
        
        [Test]
        public void ShouldCreateObjectWithNoCtor_Fails()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            resolver.Bind<NoCtor>().To((x) => x.CreateFrom<NoCtor>(null, null));
            
            Action a = () => resolver.Resolve<NoCtor>();
            a.ShouldThrow<MissingMethodException>();
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ShouldCreateProperties_MissingProperties(bool nulls)
        {
            var resolver = ServiceLoader.CreateLoader();
            
            resolver.Bind<MissingPropVerifier<string>>()
                .To((x) => x.CreateFrom<MissingPropVerifier<string>>(!nulls ? parms : null, !nulls ? props : null));
            
            Action a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a.ShouldNotThrow();
            resolver.Resolve<MissingPropVerifier<string>>().Data.Should().BeNull();
        }
        
        [Test]
        public void ShouldCreateProperties_Properties_Has()
        {
            var resolver = ServiceLoader.CreateLoader();
            props["Data"] = "abc";
            resolver.Bind<MissingPropVerifier<string>>()
                .To((x) => x.CreateFrom<MissingPropVerifier<string>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a.ShouldNotThrow();
            resolver.Resolve<MissingPropVerifier<string>>().Data.Should().Be("abc");
        }
        
        [Test]
        public void ShouldCreateProperties_Properties_Has_Case()
        {
            var resolver = ServiceLoader.CreateLoader();
            props["Data"] = "abc";
            resolver.Bind<MissingPropVerifier<string>>()
                .To((x) => x.CreateFrom<MissingPropVerifier<string>>(parms, props));
            
            Action a = () => resolver.Resolve<MissingPropVerifier<string>>();
            a.ShouldNotThrow();
            resolver.Resolve<MissingPropVerifier<string>>().Data.Should().Be("abc");
        }
        
        [TestCase(typeof(int?), true)]
        [TestCase(typeof(double?), true)]
        [TestCase(typeof(double), false)]
        [TestCase(typeof(string), false)]
        [TestCase(typeof(NoCtor), false)]
        [TestCase(typeof(IService1), false)]
        [TestCase(typeof(Service1), false)]
        public void Should_GetIsNullableType(Type type, bool isNullableType)
        {
            type.IsNullableValueType().Should().Be(isNullableType);
        }
        
        private class NoCtor
        {
            private NoCtor()
            {
                
            }
        }
        
        private class MissingFieldVerifier<T>
        {
            private readonly T data;

            public MissingFieldVerifier(T data)
            {
                this.data = data;
            }

            public T Data
            {
                get { return this.data; }
            }
        }
        
        public interface IMissingPropVerifier<T>
        {
            T Data { get; }
        }
        
        private class MissingPropVerifier<T> : IMissingPropVerifier<T>
        {
            public T Data { get; set; }
        }
        
        private interface IService1
        {
            Service2 Inner { get; }

            string Name { get; }
        }
        
        private class Service1 : IService1
        {
            private readonly string name;

            private readonly Service2 inner;
            
            public Service1(Service2 inner, string name)
            {
                this.inner = inner;
                this.name = name;
            }

            public Service2 Inner
            {
                get { return inner; }
            }

            public string Name
            {
                get { return name; }
            }
        }
        
        private class Service2
        {
            private readonly string name;

            public Service2(string name)
            {
                this.name = name;
            }

            public string Name
            {
                get { return name; }
            }
        }
        
        public class NonStringable
        {
            public override string ToString()
            {
                throw new FormatException("");
            }
        }
    }
}