﻿namespace Base2art.ComponentModel.Contrib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.ComponentModel;
    using FluentAssertions;
    using NUnit.Framework;
    using Base2art.ComponentModel.Fixtures.AsyncExample;
    
    [TestFixture]
    public class InterfaceBasedCreatorFeature
    {
        private readonly Dictionary<string, object> parms = new Dictionary<string, object>();

        private readonly Dictionary<string, object> props = new Dictionary<string, object>();

        [SetUp]
        public void BeforeEach()
        {
            props.Clear();
        }

        [Test]
        public void ShouldResolve()
        {
            var resolver = ServiceLoader.CreateLoader();
            parms["name"] = "MyName";
            var parms1 = parms.Copy();
            resolver.Bind<IService1>().To(x => x.CreateFrom<Service1>(parms1, props.Copy()));
            parms.Clear();
            parms["name"] = "OtherName";
            resolver.Bind<IService2>().To(x => x.CreateFrom<Service2>(parms, props.Copy()));
            IService1 s = (IService1)resolver.Resolve(typeof(IService1));
            Assert.AreEqual(s.Inner.Name, "OtherName");
            Assert.AreEqual(s.Name, "MyName");
        }

        [Test]
        public void ShouldResolveInterface()
        {
            var resolver = ServiceLoader.CreateLoader();
            parms["name"] = "MyName";
            var parms1 = parms.Copy();
            resolver.Bind<IService1>().To(x => x.CreateFrom<Service1>(parms1, props.Copy()));
            parms.Clear();
            parms["name"] = "OtherName";
            resolver.Bind<IService2>().To(x => x.CreateFrom<Service2>(parms, props.Copy()));
            IService1 s = (IService1)resolver.Resolve(typeof(IService1));
            Assert.AreEqual(s.Inner.Name, "OtherName");
            Assert.AreEqual(s.Name, "MyName");
        }

        [Test]
        public void ShouldRegisterAndResolve()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            parms["name"] = "MyName";
            var parms1 = parms.Copy();
            parms.Clear();
            parms["name"] = "OtherName";
            
            resolver.Register(typeof(IService1), typeof(Service1), parms1, props, false);
            resolver.Register(typeof(IService2), typeof(Service2), parms, props, false);
            
            IService1 s = (IService1)resolver.Resolve(typeof(IService1));
            Assert.AreEqual(s.Inner.Name, "OtherName");
            Assert.AreEqual(s.Name, "MyName");
            s = (IService1)resolver.Resolve(typeof(IService1));
            Assert.AreEqual(s.Inner.Name, "OtherName");
            Assert.AreEqual(s.Name, "MyName");
            
            var s1 = (IService2)resolver.Resolve(typeof(IService2));
            Assert.AreEqual(s1.Name, "OtherName");
        }

        [Test]
        public void ShouldRegisterAndResolveArray()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            parms["name"] = "MyName";
            var parms1 = parms.Copy();
            parms.Clear();
            parms["name"] = "OtherName 1";
            var parms2 = parms.Copy();
            parms.Clear();
            parms["name"] = "OtherName 2";
            var parms3 = parms.Copy();
            
            resolver.Register(typeof(ServiceTakesArray1), typeof(ServiceTakesArray1), parms1, props, false);
            resolver.Register(typeof(IService2), typeof(Service2), parms2, props, false);
            resolver.Register(typeof(IService2), typeof(Service2), parms3, props, false);
            
            var s = resolver.Resolve<ServiceTakesArray1>();
            Assert.AreEqual(s.Inner[0].Name, "OtherName 1");
            Assert.AreEqual(s.Inner[1].Name, "OtherName 2");
            Assert.AreEqual(s.Name, "MyName");
            
            s = resolver.Resolve<ServiceTakesArray1>();
            Assert.AreEqual(s.Inner[0].Name, "OtherName 1");
            Assert.AreEqual(s.Inner[1].Name, "OtherName 2");
            Assert.AreEqual(s.Name, "MyName");
        }
        
        [Test]
        public void ShouldRegisterAndResolve_SpecificExample()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            resolver.Register(typeof(ILogger), typeof(Logger), parms, props, false);
            resolver.Register(typeof(IUserService), typeof(UserService), parms, props, false);
            resolver.Register(typeof(ICacheService), typeof(CacheService), parms, props, false);
            resolver.Register(typeof(PersonService), typeof(PersonService), parms, props, false);
            
            new Action(() => resolver.Resolve(typeof(PersonService)))();//.ShouldNotThrow<CircularDependencyException>();
            new Action(() => resolver.Resolve(typeof(PersonService)))();//.ShouldNotThrow<CircularDependencyException>();
            
            //            Creator.CreateFromInternal(
            //                typeof(PersonService),
            //                resolver,
            //                (x) => false,
            //                (x) => null);
        }
        
        [Test]
        public void ShouldRegisterAndResolve_SpecificExample_1()
        {
            var resolver = ServiceLoader.CreateLoader();
            
            resolver.Register(typeof(ILogger), typeof(Logger), parms, props, false);
            resolver.Register(typeof(IUserService), typeof(UserService), parms, props, false);
            resolver.Register(typeof(ICacheService), typeof(CacheService), parms, props, false);
            //            resolver.Register(typeof(PersonService), typeof(PersonService), parms, props, false);
            
            //            new Action(() => resolver.Resolve(typeof(PersonService)))();//.ShouldNotThrow<CircularDependencyException>();
            //            new Action(() => resolver.Resolve(typeof(PersonService)))();//.ShouldNotThrow<CircularDependencyException>();
            resolver.Bind<PersonService>()
                .To(x => x.CreateFrom<PersonService>(new Dictionary<string, object>(), new Dictionary<string, object>()));
            //            DynamicRegistrar.CreateFrom(
            //                var p = Creator.CreateFromInternal(
            //                    typeof(PersonService),
            //                    resolver,
            //                    (x) => false,
            //                    (x) => null);
            resolver.Resolve<PersonService>().Should().NotBeNull();
        }
        
        [Test]
        public async void AsyncTry()
        {
            if (Environment.ProcessorCount <= 1)
            {
                Assert.Fail("This test requires 2 cpus.");
            }
            
            var resolver = ServiceLoader.CreateLoader();
            
            resolver.Register(typeof(ILogger), typeof(Logger), parms, props, false);
            resolver.Register(typeof(IUserService), typeof(UserService), parms, props, false);
            resolver.Register(typeof(ICacheService), typeof(CacheService), parms, props, false);
            resolver.Register(typeof(PersonServiceWithSlowDown), typeof(PersonServiceWithSlowDown), parms, props, false);
            
            var call = new Func<object>(() => resolver.Resolve(typeof(PersonServiceWithSlowDown)));
            
            var task1 = Task.Run(call);
            var task2 = Task.Run(call);
            await task2;
            await task1;
        }
        
        //        [Test]
        //        public void ShouldResolveInterface1()
        //        {
        //            var resolver = ServiceLoader.CreateLoader();
        //
        //            resolver.Bind<IService1>()
        //                .To(x => x.CreateFrom<IService1>(new Dictionary<string, object> { {
        //                                                         "name",
        //                                                         "MyName"
        //                                                     }
        //                                                 }));
        //
        //            resolver.Bind<Service2>()
        //                .To(x => x.CreateFrom<Service2>(new Dictionary<string, object> { {
        //                                                        "name",
        //                                                        "OtherName"
        //                                                    }
        //                                                }));
        //
        //            IService1 s = (IService1)resolver.Resolve(typeof(IService1));
        //            Assert.AreEqual(s.Inner.Name, "OtherName");
        //        }
        //
        //        [Test]
        //        public void ShouldResolveInterface2()
        //        {
        //            var resolver = ServiceLoader.CreateLoader();
        //
        //            resolver.Bind<IService1>()
        //                .To(x => x.CreateFrom<IService1>(new Dictionary<string, object> { {
        //                                                         "name",
        //                                                         "MyName"
        //                                                     }
        //                                                 }));
        //
        //            resolver.Bind<Service2>()
        //                .To(x => x.CreateFrom<Service2>(new Dictionary<string, object> { {
        //                                                        "name",
        //                                                        "OtherName"
        //                                                    }
        //                                                }));
        //            try
        //            {
        //                IService1 s = (IService1)resolver.Resolve(typeof(IService1));
        //                Assert.AreEqual(s.Inner.Name, "OtherName");
        //            }
        //            catch (Exception)
        //            {
        //            }
        //
        //            IService1 s1 = (IService1)resolver.Resolve(typeof(IService1));
        //            Assert.AreEqual(s1.Inner.Name, "OtherName");
        //        }
        private interface IService1
        {
            IService2 Inner
            {
                get;
            }

            string Name
            {
                get;
            }
        }
        
        private interface IService2
        {
            string Name
            {
                get;
            }
        }

        private class Service1 : IService1
        {
            private readonly string name;

            private readonly IService2 inner;

            public Service1(IService2 inner, string name)
            {
                this.inner = inner;
                this.name = name;
            }

            public IService2 Inner
            {
                get
                {
                    return inner;
                }
            }

            public string Name
            {
                get
                {
                    return name;
                }
            }
        }

        private class ServiceTakesArray1// : IService1
        {
            private readonly string name;

            private readonly IService2[] inner;

            public ServiceTakesArray1(IService2[] inner, string name)
            {
                this.inner = inner;
                this.name = name;
            }

            public IService2[] Inner
            {
                get
                {
                    return this.inner;
                }
            }

            public string Name
            {
                get
                {
                    return name;
                }
            }
        }

        private class Service2 : IService2
        {
            private readonly string name;

            public Service2(string name)
            {
                this.name = name;
            }

            public string Name
            {
                get
                {
                    return name;
                }
            }
        }
    }
}


