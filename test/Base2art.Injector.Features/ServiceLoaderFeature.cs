﻿namespace Base2art.ComponentModel
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using Base2art.Serialization.ItemSerialization;
    using Base2art.ComponentModel.Fixtures;
    using Base2art.ComponentModel.Fixtures.AsyncExample;
    using Base2art.ComponentModel.Fixtures.NestingOfTypes;

    using FluentAssertions;

    
    using NUnit.Framework;

    [TestFixture]
    public class ServiceLoaderFeature
    {
        [Test]
        public void ShouldGetInstanceThrowExceptionWhenTypeNotRegistered()
        {
            var loader = ServiceLoader.CreateLoader();

            new Action(() => loader.Resolve<ITestFactory>()).ShouldThrow<KeyNotFoundException>();
        }

        [Test]
        public void ShouldGetInstancesThrowExceptionWhenTypeNotRegistered()
        {
            var loader = ServiceLoader.CreateLoader();

            new Action(() => loader.ResolveAll<ITestFactory>()).ShouldThrow<KeyNotFoundException>();
        }
        
        [Test]
        public void ShouldGetInstanceNotThrowExceptionWhenTypeNotRegisteredAndSpecifiyNoException()
        {
            var loader = ServiceLoader.CreateLoader();

            loader.Resolve<ITestFactory>(true).Should().BeNull();
        }
        
        [Test]
        public void ShouldGetNamedInstanceNotThrowExceptionWhenTypeNotRegisteredAndSpecifiyNoException()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("Scott").To(x=> new TestFactory(9));
            
            loader.ResolveByNamed<ITestFactory>("Scott", true).GetInstanceNumber().Should().Be(9);
            loader.ResolveByNamed<ITestFactory>("Matt", true).Should().BeNull();
            
            loader.ResolveByNamed<ITestCaseData>("Matt", true).Should().BeNull();
        }

        [Test]
        public void ShouldGetInstancesNotThrowExceptionWhenTypeNotRegisteredAndSpecifiyNoException()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.ResolveAll<ITestFactory>(true).Should().Equal(new ITestFactory[0]);
        }
        
        [Test]
        public void ShouldVerifyHasTypeNested()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => new TestFactory(9));
            loader.Bind<ITestFactoryProxy>().To(x => new TestFactoryProxy(x.Resolve<ITestFactory>()));
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(9);
        }
        
        [Test]
        public void ShouldVerifyHasTypeNestedReturnsNull()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactoryProxy>().To(x => new TestFactoryProxy(x.Resolve<ITestFactory>(true)));
            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber().Should().Be(-85);
        }
        
        [Test]
        public void ShouldVerifyGettingItemByConcreteType()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<TestFactoryProxy>().To(x => new TestFactoryProxy(x.Resolve<ITestFactory>(true)));
            loader.Resolve<TestFactoryProxy>().GetInstanceNumber().Should().Be(-85);
        }

        [Test]
        public void ShouldLoadTypeByDelegateWithSingleton()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>()
                .As(ServiceLoaderBindingType.Singleton)
                .To(x => new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            ((ITestFactory)loader.Resolve(typeof(ITestFactory))).GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());
        }

        [Test]
        public void ShouldLoadTypeByDelegateWithInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>()
                .As(ServiceLoaderBindingType.Instance)
                .To(x => new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                .Should().NotBe(loader.Resolve<ITestFactory>().GetInstanceNumber());
        }

        [Test]
        public void ShouldLoadTypeByInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Test]
        public void ShouldLoadTypeByInstanceUsingConcreteCreator()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().ToInstanceCreator<TestFactory>();

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber() - 1);

            loader.VerifyAll();
        }

        [Test]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithZeroCtors()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().ToInstanceCreator<TestFactoryNoCtor>();

            Assert.Throws<MissingMethodException>(() => loader.Resolve<ITestFactory>());
        }

        [Test]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithNoDefault()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactoryProxy>().ToInstanceCreator<TestFactoryProxy>();
            loader.Bind<ITestFactory>().To(x => new TestFactory(7));

            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactoryProxy>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Test]
        public void Bug_WithServiceLoaderAndSingleton()
        {
            {
                var loader = ServiceLoader.CreateLoader();
                loader.Bind<Interface1>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL1>();
                loader.Bind<Interface2>().ToInstanceCreator<KlassL2>();
                loader.Bind<Interface3>().ToInstanceCreator<KlassL3>();

                loader.Resolve<Interface3>().Should().NotBeNull();
            }
            
            {
                var loader = ServiceLoader.CreateLoader();
                loader.Bind<Interface1>().ToInstanceCreator<KlassL1>();
                loader.Bind<Interface2>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL2>();
                loader.Bind<Interface3>().ToInstanceCreator<KlassL3>();

                loader.Resolve<Interface3>().Should().NotBeNull();
            }
            
            {
                var loader = ServiceLoader.CreateLoader();
                loader.Bind<Interface1>().ToInstanceCreator<KlassL1>();
                loader.Bind<Interface2>().ToInstanceCreator<KlassL2>();
                loader.Bind<Interface3>().As(ServiceLoaderBindingType.Singleton).ToInstanceCreator<KlassL3>();

                loader.Resolve<Interface3>().Should().NotBeNull();
            }
            
        }

        [Test]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithNoDefault_AndProxyByDelegate()
        {
            var loader = ServiceLoader.CreateLoader();
            
            loader.Bind<ITestFactory>()
                .As(ServiceLoaderBindingType.Singleton)
                .To(new TestFactory(7));

            loader.Bind<ITestFactoryProxy>()
                .As(ServiceLoaderBindingType.Instance)
                .ToInstanceCreator<TestFactoryProxy>();
//
            //            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber()
            //                .Should().Be(loader.Resolve<ITestFactoryProxy>().GetInstanceNumber());

            loader.VerifyAll();
            
            loader.Resolve<ITestFactoryProxy>().Should().NotBeNull();
        }

        [Test]
        public void ShouldLoadTypeByInstanceNestedDependencies()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactoryGrouper>().ToInstanceCreator<TestFactoryGrouper>();
            loader.Bind<ITestFactoryProxy>().ToInstanceCreator<TestFactoryProxy>();
            loader.Bind<ITestFactory>().To(x => new TestFactory(7));

            loader.Resolve<ITestFactoryProxy>().GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactoryProxy>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Test]
        public void ShouldLoadTypeByInstanceByClass()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());

            loader.Resolve(typeof(ITestFactory).GetClass().As<ITestFactory>()).GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Test]
        public void ShouldLockButAbleToGetInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());

            loader.Seal();

            new Action(() => loader.Bind<ITestFactory>().To(new TestFactory()))
                .ShouldThrow<ReadOnlyException>();

            loader.Resolve<ITestFactory>().GetInstanceNumber()
                .Should().Be(loader.Resolve<ITestFactory>().GetInstanceNumber());

            loader.VerifyAll();
        }

        [Test]
        public void ShouldRegisterMultipleInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());
            loader.Bind<ITestFactory>().To(new AlternateTestFactory());

            loader.Seal();

            var factories = loader.ResolveAll<ITestFactory>();
            factories[0].Should().BeOfType<TestFactory>();
            factories[1].Should().BeOfType<AlternateTestFactory>();

            factories[1].GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Test]
        public void ShouldRegisterMultipleInstancesForClass()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(new TestFactory());
            loader.Bind<ITestFactory>().To(new AlternateTestFactory());

            loader.Seal();

            var factories = loader.ResolveAll(typeof(ITestFactory).GetClass().As<ITestFactory>());
            factories[0].Should().BeOfType<TestFactory>();
            factories[1].Should().BeOfType<AlternateTestFactory>();

            factories[1].GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Test]
        public void ShouldGetInstanceByName()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("TF").To(new TestFactory());

            loader.Bind<ITestFactory>()
                .Named("ATF")
                .As(ServiceLoaderBindingType.Instance)
                .To(x => new AlternateTestFactory());

            loader.Seal();

            new Action(() => loader.ResolveByNamed<ITestFactory>("ABF")).ShouldThrow<KeyNotFoundException>();
            new Action(() => loader.ResolveByNamed(typeof(ITestFactory), "ABF")).ShouldThrow<KeyNotFoundException>();

            var value = loader.ResolveByNamed<ITestFactory>("ATF");
            value.Should().BeOfType<AlternateTestFactory>();
            value.GetInstanceNumber().Should().Be(42);

            var valueByType = loader.ResolveByNamed(typeof(ITestFactory), "ATF");
            valueByType.Should().BeOfType<AlternateTestFactory>();
            ((ITestFactory)valueByType).GetInstanceNumber().Should().Be(42);
            
            loader.VerifyAll();
        }

        [Test]
        public void ShouldGetInstanceByNameForClass()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("TF").To(new TestFactory());

            loader.Bind<ITestFactory>()
                .Named("ATF")
                .As(ServiceLoaderBindingType.Instance)
                .To(x => new AlternateTestFactory());

            loader.Seal();

            var klazz = typeof(ITestFactory).GetClass().As<ITestFactory>();
            new Action(() => loader.ResolveByNamed(klazz, "ABF")).ShouldThrow<KeyNotFoundException>();

            var value = loader.ResolveByNamed(klazz, "ATF");
            value.Should().BeOfType<AlternateTestFactory>();

            value.GetInstanceNumber().Should().Be(42);

            loader.VerifyAll();
        }

        [Test]
        public void ShouldHaveUniqueNameByType()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>()
                .As(ServiceLoaderBindingType.Singleton)
                .Named("TF")
                .To(new TestFactory());

            new Action(
                () =>
                loader.Bind<ITestFactory>()
                .As(ServiceLoaderBindingType.Instance)
                .Named("TF")
                .To(x => new AlternateTestFactory()))
                .ShouldThrow<DuplicateNameException>();
        }

        /*
        [Test]
        public void ShouldHaveStaticApi()
        {
            new Action(() => ServiceLoader.GetInstance<ITestFactory>()).ShouldThrow<InvalidOperationException>();
            new Action(() => ServiceLoader.GetInstanceNamed<ITestFactory>("abc")).ShouldThrow<InvalidOperationException>();
            new Action(() => ServiceLoader.GetInstances<ITestFactory>()).ShouldThrow<InvalidOperationException>();

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.Resolve<ITestFactory>()).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstance<ITestFactory>();
                mock.Verify(x => x.Resolve<ITestFactory>());
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveByNamed<ITestFactory>("abc")).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstanceNamed<ITestFactory>("abc");
                mock.Verify(x => x.ResolveByNamed<ITestFactory>("abc"));
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveAll<ITestFactory>()).Returns(new ITestFactory[] { new TestFactory() });
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstances<ITestFactory>();
                mock.Verify(x => x.ResolveAll<ITestFactory>());
            }
            
            // Allow true / false
            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.Resolve<ITestFactory>()).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstance<ITestFactory>(true);
                mock.Verify(x => x.Resolve<ITestFactory>(true));
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveByNamed<ITestFactory>("abc")).Returns(new TestFactory());
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstanceNamed<ITestFactory>("abc", true);
                mock.Verify(x => x.ResolveByNamed<ITestFactory>("abc", true));
            }

            {
                var mock = new Mock<IServiceLoaderInjector>();
                mock.Setup(x => x.ResolveAll<ITestFactory>()).Returns(new ITestFactory[] { new TestFactory() });
                ServiceLoader.PrimaryLoader = mock.Object;
                ServiceLoader.GetInstances<ITestFactory>(true);
                mock.Verify(x => x.ResolveAll<ITestFactory>(true));
            }
        }
         */
        [Test]
        public void ShouldGetNestedInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => new TestFactory());
            loader.Bind<IChildTestFactory>()
                .To(x => new ChildTestFactory(x.Resolve<ITestFactory>()));
        }

        [Test]
        public void ShouldGetNestedInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => new TestFactory(99));
            loader.Bind<ITestFactory>().To(x => new TestFactory(100));
            loader.Bind<IChildTestFactory>()
                .To(x => new ChildTestFactoryMultiple(x.ResolveAll<ITestFactory>()));
            loader.Resolve<IChildTestFactory>().GetInstanceName().Should().Be("99100");
        }

        [Test]
        public void ShouldGetNestedNamedInstance()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("s").To(x => new TestFactory(99));
            loader.Bind<ITestFactory>().Named("t").To(x => new TestFactory(100));
            loader.Bind<IChildTestFactory>()
                .To(x => new ChildTestFactory(x.ResolveByNamed<ITestFactory>("t")));
            loader.Resolve<IChildTestFactory>().GetInstanceName().Should().Be("100");
        }

        [Test]
        public void ShouldGetWhenNestedNamedInstanceIsNull()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().Named("s").To(x => new TestFactory(99));
            loader.Bind<ITestFactory>().Named("t").To(x => new TestFactory(100));
            loader.Bind<ITestFactoryProxy>()
                .Named("Alpha")
                .To(x => new TestFactoryProxy(x.ResolveByNamed<ITestFactory>("NULL", true)));
            loader.Bind<ITestFactoryProxy>()
                .Named("Beta")
                .To(x => new TestFactoryProxy(x.ResolveByNamed<ITestFactory>("NULL", false)));
            loader.ResolveByNamed<ITestFactoryProxy>("Alpha").GetInstanceNumber().Should().Be(-85);
            
            new Action(() => loader.ResolveByNamed<ITestFactoryProxy>("Beta")).ShouldThrow<KeyNotFoundException>();
        }

        [Test]
        public void ShouldHandleSuperCircularReference()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<ITestFactory>().To(x => x.Resolve<ITestFactory>());
            
            new Action(() => loader.Resolve<ITestFactory>()).ShouldThrow<CircularDependencyException>();
        }

        [Test]
        public void ShouldLoadTypeByInstanceUsingConcreteCreatorWithCircularDependency()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<A>().ToInstanceCreator<A>();
            loader.Bind<B>().ToInstanceCreator<B>();

            Assert.Throws<CircularDependencyException>(() => loader.Resolve<A>());
        }

        [Test]
        public void ShouldGetCircularInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IInterface1>().To(x => new Implementation1(x.Resolve<IInterface2>()));
            loader.Bind<IInterface2>().To(x => new Implementation2(x.Resolve<IInterface1>()));
            new Action(() => loader.Resolve<IInterface2>()).ShouldThrow<CircularDependencyException>();
        }

        [Test]
        public void ShouldGetWithoutCircularInstances()
        {
            var loader = ServiceLoader.CreateLoader();
            
            loader.Bind<IUserService>().ToInstanceCreator<UserService>();
            loader.Bind<ICacheService>().ToInstanceCreator<CacheService>();
            loader.Bind<PersonService>().ToInstanceCreator<PersonService>();
            loader.Bind<ILogger>().ToInstanceCreator<Logger>();
            new Action(() => loader.Resolve(typeof(PersonService))).ShouldNotThrow<CircularDependencyException>();
            new Action(() => loader.Resolve(typeof(PersonService))).ShouldNotThrow<CircularDependencyException>();
        }

        [Test]
        public void ShouldVerifyAllCircular()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IInterface1>().To(x => new Implementation1(x.Resolve<IInterface2>()));
            loader.Bind<IInterface2>().To(x => new Implementation2(x.Resolve<IInterface1>()));
            new Action(loader.VerifyAll).ShouldThrow<CircularDependencyException>();
        }

//        [Test]
//        public void ShouldThrowExceptionWhenCallVerifyAllOnChild()
//        {
//            var loader = ServiceLoader.CreateLoader();
//            loader.Bind<IInterface1>().To(x => {x.VerifyAll(); return null;});
//            new Action(() => loader.Resolve<IInterface1>()).ShouldThrow<NotImplementedException>();
//        }

        [Test]
        public void ShouldVerifyAllIncomplete()
        {
            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IInterface2>().To(x => new Implementation2(x.Resolve<IInterface1>()));
            new Action(loader.VerifyAll).ShouldThrow<KeyNotFoundException>();
        }

        [Test]
        public void ShouldSerializeAndDeserialize()
        {
            var m = new CircularDependencyException();
            var ser = new BinaryItemSerializer<CircularDependencyException>();
            ser.Deserialize(ser.Serialize(m)).Message.Should().Be(m.Message);

            var m1 = new CircularDependencyException("ABC");
            ser.Clone(m1).Message.Should().Be(m1.Message);

            var m2 = new CircularDependencyException("ABCD", new ApplicationException("1"));
            var m3 = ser.Clone(m2);
            m3.Message.Should().Be(m2.Message);
            m3.InnerException.Message.Should().Be(m3.InnerException.Message);
        }
        
        [Test]
        public void ShouldBindModule_NonGeneric_Sealed()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.Seal();
            Action mut = () => injector.BindModule(new CustomModule());
            mut.ShouldThrow<ReadOnlyException>();
        }
        
        [Test]
        public void ShouldBindModule_NonGeneric_NullRef()
        {
            var injector = ServiceLoader.CreateLoader();
            Action mut = () => injector.BindModule(null);
            mut.ShouldThrow<ArgumentNullException>();
        }
        
        [Test]
        public void ShouldBindModule_NonGeneric()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.BindModule(new CustomModule());
            injector.Resolve<ITestFactory>().GetInstanceNumber().Should().BeGreaterOrEqualTo(1);
        }
        
        [Test]
        public void ShouldBindModule_Generic_Sealed()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.Seal();
            Action mut = () => injector.BindModule<CustomModule>();
            mut.ShouldThrow<ReadOnlyException>();
        }
        
        [Test]
        public void ShouldBindModule_Generic()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.BindModule<CustomModule>();
            injector.Resolve<ITestFactory>().GetInstanceNumber().Should().Be(1);
        }
        
        [Test]
        public void Should_FailToResolve()
        {
            var injector = ServiceLoader.CreateLoader();
            Action mut = () => injector.Resolve<ITestFactory>();
            mut.ShouldThrow<KeyNotFoundException>().And.Message.Should().Contain("ITestFactory");
        }
        
        [Test]
        public void Should_FailToResolve_Nested()
        {
            var injector = ServiceLoader.CreateLoader();
            injector.Bind<Interface2>().ToInstanceCreator<KlassL2>();
            Action mut = () => injector.Resolve<Interface2>();
            mut.ShouldThrow<KeyNotFoundException>().And.Message.Should().Contain("Interface1");
        }
        
        public class CustomModule : IServiceLoaderInjectorModule
        {
            public void Configure(IBindableServiceLoaderInjector serviceLoaderInjector)
            {
                serviceLoaderInjector.Bind<ITestFactory>().To(new TestFactory());
            }
        }
        
        private class A
        {
            public A(B b) { }
        }
        
        private class B
        {
            public B(A a) { }
        }
    }
}
