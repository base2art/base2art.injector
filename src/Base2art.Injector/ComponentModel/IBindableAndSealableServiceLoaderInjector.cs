﻿namespace Base2art.ComponentModel
{
    public interface IBindableAndSealableServiceLoaderInjector : IBindableServiceLoaderInjector
    {
        // WARNING DO NOT USE, IN PRODUCTION It RUINS
        // LAZY LOADING
        void VerifyAll();

        void Seal();
    }
}
