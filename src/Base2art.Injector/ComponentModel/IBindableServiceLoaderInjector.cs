﻿namespace Base2art.ComponentModel
{
    public interface IBindableServiceLoaderInjector : IServiceLoaderInjector
    {
        IServiceLoaderBinding<T> Bind<T>();
        
        IBindableServiceLoaderInjector BindModule(IServiceLoaderInjectorModule module);
        
        IBindableServiceLoaderInjector BindModule<TModule>()
            where TModule : IServiceLoaderInjectorModule, new();
    }
}
