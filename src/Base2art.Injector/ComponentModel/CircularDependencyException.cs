namespace Base2art.ComponentModel
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class CircularDependencyException : Exception
    {
        public CircularDependencyException()
        {
        }

        public CircularDependencyException(string message)
            : base(message)
        {
        }

        public CircularDependencyException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CircularDependencyException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}