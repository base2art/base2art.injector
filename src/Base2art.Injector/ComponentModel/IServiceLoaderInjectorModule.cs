﻿namespace Base2art.ComponentModel
{
    public interface IServiceLoaderInjectorModule
    {
        void Configure(IBindableServiceLoaderInjector serviceLoaderInjector);
    }
}
