﻿namespace Base2art.ComponentModel.Contrib
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Base2art.ComponentModel;
    
    public static class DynamicRegistrar
    {
        public static IEnumerable<Type> GetAllInterfacesImplementedBy<T>()
        {
            return typeof(T).GetInterfaces();
        }
        
        public static bool IsNullableValueType(this Type type)
        {
            return type.IsValueType && (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
        
        public static T CreateFrom<T>(
            this IServiceLoaderInjector config,
            IDictionary<string, object> inputParameters,
            IDictionary<string, object> properties)
        {
            var obj = (T)Creator.CreateFromInternal(
                typeof(T),
                config,
                s => inputParameters.ContainsKey(s.Name),
                s => Creator.Convert(inputParameters, () => s.Name, () => s.ParameterType));
            Creator.SetProperties(properties, obj);
            return obj;
        }

        public static void RegisterInstance<T>(
            this IBindableAndSealableServiceLoaderInjector injector,
            T instance,
            bool registerImplementedInterfaces)
        {
            var items = new List<Tuple<Type, object>>();
            items.Add(Tuple.Create(typeof(T), (object)instance));
            
            if (registerImplementedInterfaces)
            {
                foreach (var item in GetAllInterfacesImplementedBy<T>())
                {
                    items.Add(Tuple.Create(item, (object)instance));
                }
            }
            
            foreach (var item in items)
            {
                var parm1 = ServiceLoaderBindingType.Singleton;
                var rez = typeof(DynamicRegistrar).CallStatic(new[] { item.Item1 }, "Register1NonGeneric", new object[] { injector, parm1 });
                typeof(DynamicRegistrar).CallStatic(new[] { item.Item1 }, "Register3NonGeneric", new object[] { rez, item.Item2 });
            }
        }
        
        public static void Register(
            this IBindableServiceLoaderInjector injector,
            Type requested,
            Type fulfilling,
            IDictionary<string, object> parameters,
            IDictionary<string, object> properties,
            bool isSingleton)
        {
            var parm1 = isSingleton ? ServiceLoaderBindingType.Singleton : ServiceLoaderBindingType.Instance;
            var item = typeof(DynamicRegistrar).CallStatic(new[] { requested }, "Register1NonGeneric", new object[] { injector, parm1 });
            
            Func<IDictionary<string, object>, IDictionary<string, object>> c = (x) => x ?? new Dictionary<string, object>();
            typeof(DynamicRegistrar).CallStatic(new[] { requested,  fulfilling }, "Register2NonGeneric", new object[] { injector, item, c(parameters), c(properties) });
        }
        
        public static IServiceLoaderBinding<T> Register1NonGeneric<T>(
            IBindableServiceLoaderInjector injector,
            ServiceLoaderBindingType bindingType)
        {
            return injector.Bind<T>()
                .As(bindingType);
        }
        
        public static void Register2NonGeneric<T, TConcrete>(
            IBindableServiceLoaderInjector injector,
            IServiceLoaderBinding<T> binding,
            IDictionary<string, object> inputParms,
            IDictionary<string, object> properties)
            where TConcrete : T
        {
            binding.To((injector1) => injector.CreateFrom<TConcrete>(inputParms, properties));
        }
        
        public static void Register3NonGeneric<T>(IServiceLoaderBinding<T> binding, T instance)
        {
            binding.To(instance);
        }
    }
}
