﻿namespace Base2art.ComponentModel.Contrib
{
    using System;
    using System.Reflection;
    
    internal static class GenericProxy
    {
        public static object CallStatic(this Type target, Type[] genericTypes, string methodName, params object[] objs)
        {
            MethodInfo method2 = target.GetMethod(methodName);
            MethodInfo generic2 = method2.MakeGenericMethod(genericTypes);
            
            return generic2.Invoke(null, objs);
        }
    }
}
