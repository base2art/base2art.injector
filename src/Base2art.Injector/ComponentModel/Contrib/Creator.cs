﻿namespace Base2art.ComponentModel.Contrib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using Base2art.ComponentModel;
    
    internal static class Creator
    {
        public static Array GetServicesArray(this IServiceLoaderInjector resolver, Type type)
        {
            var optionsObjs = resolver.ResolveAll(type).ToArray();
            var newArray = Array.CreateInstance(type, optionsObjs.Length);
            Array.Copy(optionsObjs, newArray, optionsObjs.Length);
            return newArray;
        }
        
        public static object CreateFromInternal(
            this Type t,
            IServiceLoaderInjector config,
            Func<ParameterInfo, bool> hasParam,
            Func<ParameterInfo, object> getParam)
        {
            ConstructorInfo[] ctors = t.GetConstructors();
            if (ctors.Length == 0)
            {
                return Activator.CreateInstance(t);
            }
            
            List<ConstructorInfo> items = (from x in ctors
                                           orderby x.GetParameters().Length
                                           select x).ToList<ConstructorInfo>();
            ConstructorInfo ctor = items[0];
            ParameterInfo[] parms = ctor.GetParameters();
            int length = parms.Length;
            if (length == 0)
            {
                return Activator.CreateInstance(t);
            }
            
            object[] obj = new object[length];
            for (int i = 0; i < obj.Length; i++)
            {
                var parameterInfo = parms[i];
                if (hasParam(parameterInfo))
                {
                    obj[i] = getParam(parameterInfo);
                }
                else
                {
                    if (parameterInfo.ParameterType == typeof(string))
                    {
                        throw new MissingFieldException(t.FullName, parameterInfo.Name);
                    }
                    
                    if (parameterInfo.ParameterType.IsPrimitive)
                    {
                        throw new MissingFieldException(t.FullName, parameterInfo.Name);
                    }
                    
                    if (parameterInfo.ParameterType.IsNullableValueType())
                    {
                        throw new MissingFieldException(t.FullName, parameterInfo.Name);
                    }
                    
                    var parmType = parameterInfo.ParameterType;
                    if (parmType.IsArray)
                    {
                        var innerParmType = parmType.GetElementType();
                        obj[i] = config.GetServicesArray(innerParmType);
                    }
                    else
                    {
                        obj[i] = config.Resolve(parmType);
                    }
                }
            }
            
            return (object)Activator.CreateInstance(t, obj);
        }

        public static void SetProperties(IDictionary<string, object> properties, object obj)
        {
            if (properties == null)
            {
                return;
            }
            
            if (properties.Count > 0)
            {
                var objType = obj.GetType();
                foreach (var element in properties)
                {
                    var prop = objType.GetProperty(element.Key);
                    var propSet = prop.GetSetMethod();
                    propSet.Invoke(obj, new[] { Convert(properties, () => prop.Name, () => prop.PropertyType) });
                }
            }
        }

        public static object Convert(IDictionary<string, object> inputParms, Func<string> nameGetter, Func<Type> typeGetter)
        {
            var value = inputParms[nameGetter()];
            var type = typeGetter();
            
            if (value == null)
            {
                return null;
            }
            
            var type2 = value.GetType();
            
            if (type2 == type)
            {
                return value;
            }
            
            var converter = TypeDescriptor.GetConverter(type);
            
            if (converter.CanConvertFrom(type2))
            {
                return converter.ConvertFrom(value);
            }
            
            var message = string.Format(CultureInfo.InvariantCulture,  "Cannot convert from '{0}' to '{1}'", type2, type);
            throw new FormatException(message);
        }
    }
}
