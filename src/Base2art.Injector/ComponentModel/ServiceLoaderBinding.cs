namespace Base2art.ComponentModel
{
    using System;
    using System.Globalization;
    using System.Linq;

    internal class ServiceLoaderBinding<T> : IServiceLoaderBinding<T>
    {
        private readonly ServiceLoaderInjector serviceLoaderInjector;

        public ServiceLoaderBinding(ServiceLoaderInjector serviceLoaderInjector)
        {
            this.serviceLoaderInjector = serviceLoaderInjector;
        }

        public ServiceLoaderBindingType BindingType { get; set; }

        public string Name { get; set; }

        protected Func<IServiceLoaderInjector, T> CreationFunction { get; set; }

        public IServiceLoaderBinding<T> As(ServiceLoaderBindingType type)
        {
            this.BindingType = type;
            return this;
        }

        public IServiceLoaderBinding<T> Named(string name)
        {
            this.Name = name;
            return this;
        }

        public void To(Func<IServiceLoaderInjector, T> creationFunction)
        {
            this.CreationFunction = creationFunction;
            this.serviceLoaderInjector.RegisterInternal(
                this.CreationFunction,
                this.Name,
                this.BindingType);
        }

        public void To<TConcrete>(TConcrete concreteValue)
            where TConcrete : T
        {
            this.CreationFunction = x => concreteValue;
            this.serviceLoaderInjector.RegisterInternal(
                this.CreationFunction,
                this.Name,
                ServiceLoaderBindingType.Singleton);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "SjY")]
        public void ToInstanceCreator<TConcrete>()
            where TConcrete : T
        {
            this.To(x => CreateInstance<TConcrete>(x));
        }
        
        private static TConcrete CreateInstance<TConcrete>(IServiceLoaderInjector injector)
        {
            var t = typeof(TConcrete);
            var ctors = t.GetConstructors();
            if (ctors.Length == 0)
            {
                return Activator.CreateInstance<TConcrete>();
            }
            
            var items = ctors.OrderBy(x => x.GetParameters().Length).ToList();
            
            var ctor = items[0];
            var parms = ctor.GetParameters();
            var length = parms.Length;
            
            if (length == 0)
            {
                return Activator.CreateInstance<TConcrete>();
            }
            
            object[] obj = new object[length];
            for (int i = 0; i < obj.Length; i++)
            {
                try
                {
                    obj[i] = injector.Resolve(parms[i].ParameterType);
                }
                catch (CircularDependencyException e)
                {
                    var errorMessage = string.Format(CultureInfo.InvariantCulture, "{0} in {1}", parms[i].ParameterType, t);
                    throw new CircularDependencyException(errorMessage, e);
                }
            }
            
            return (TConcrete)Activator.CreateInstance(t, obj);
        }
    }
}