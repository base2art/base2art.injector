namespace Base2art.ComponentModel
{
    using System;
    
    public interface IServiceLoaderInjector
    {
        object Resolve(Type contractType);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
        T Resolve<T>(IClass<T> @class);

        T Resolve<T>();

        object ResolveByNamed(Type contractType, string name);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
        T ResolveByNamed<T>(IClass<T> @class, string name);

        T ResolveByNamed<T>(string name);

        object[] ResolveAll(Type contractType);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
        T[] ResolveAll<T>(IClass<T> @class);

        T[] ResolveAll<T>();
        
        object Resolve(Type contractType, bool returnDefaultOnNotFound);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
        T Resolve<T>(IClass<T> @class, bool returnDefaultOnNotFound);

        T Resolve<T>(bool returnDefaultOnNotFound);

        object ResolveByNamed(Type contractType, string name, bool returnDefaultOnNotFound);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
        T ResolveByNamed<T>(IClass<T> @class, string name, bool returnDefaultOnNotFound);

        T ResolveByNamed<T>(string name, bool returnDefaultOnNotFound);

        object[] ResolveAll(Type contractType, bool returnDefaultOnNotFound);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", Justification = "SjY")]
        T[] ResolveAll<T>(IClass<T> @class, bool returnDefaultOnNotFound);

        T[] ResolveAll<T>(bool returnDefaultOnNotFound);
    }
}