﻿namespace Base2art.ComponentModel
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using Base2art.Collections;
    using Base2art.ComponentModel;
    using Base2art.Validation;
    
    internal class ServiceLoaderInjector : ServiceLoaderInjectorBase, IBindableAndSealableServiceLoaderInjector
    {
        private readonly MultiMap<Type, TypeInitializationData> typeCache = new MultiMap<Type, TypeInitializationData>();
        
        private bool isReadonly;

        protected virtual HasResolvedContainer Container
        {
            get { return new HasResolvedContainer(); }
        }
        
        public IServiceLoaderBinding<T> Bind<T>()
        {
            if (this.isReadonly)
            {
                throw new ReadOnlyException("This injector cannot be augmented; it is readonly");
            }

            return new ServiceLoaderBinding<T>(this);
        }

        public IBindableServiceLoaderInjector BindModule(IServiceLoaderInjectorModule module)
        {
            if (module == null)
            {
                throw new ArgumentNullException("module");
            }
            
            if (this.isReadonly)
            {
                throw new ReadOnlyException("This injector cannot be augmented; it is readonly");
            }

            module.Configure(this);
            
            return this;
        }

        public IBindableServiceLoaderInjector BindModule<TModule>()
            where TModule : IServiceLoaderInjectorModule, new()
        {
            if (this.isReadonly)
            {
                throw new ReadOnlyException("This injector cannot be augmented; it is readonly");
            }

            var module = new TModule();
            
            module.Configure(this);
            
            return this;
        }

        public void VerifyAll()
        {
            foreach (var key in this.typeCache)
            {
                this.ResolveAll(key.Key);
            }
        }

        public void Seal()
        {
            this.isReadonly = true;
        }

        internal void RegisterInternal<T>(
            Func<IServiceLoaderInjector, T> creationFunction,
            string name,
            ServiceLoaderBindingType bindingType)
        {
            var contractType = typeof(T);
            if (this.typeCache.Contains(contractType))
            {
                if (!string.IsNullOrWhiteSpace(name))
                {
                    if (this.typeCache[contractType].Any(x => x.Name == name))
                    {
                        throw new DuplicateNameException("Two items have the same name...");
                    }
                }
            }

            TypeInitializationData typeInitializationData = new TypeInitializationData(
                x =>
                {
                    try
                    {
                        return creationFunction(x);
                    }
                    catch (CircularDependencyException cde)
                    {
                        var errorMessage = string.Format(CultureInfo.InvariantCulture, "{0}", contractType);
                        throw new CircularDependencyException(errorMessage, cde);
                    }
                },
                name,
                bindingType);

            this.typeCache.Add(contractType, typeInitializationData);
        }

        protected override bool HasType(Type contractType)
        {
            return this.typeCache.Contains(contractType);
        }
        
        protected override object ResolveInternal(Type contractType, bool returnDefaultOnNotFound)
        {
            if (!this.typeCache.Contains(contractType))
            {
                throw new KeyNotFoundException("Cannot resolve type: " + contractType.FullName);
            }
            
            var typeInitializationDatum = this.typeCache[contractType].ToArray();
            var typeInitializationData = typeInitializationDatum.FirstOrDefault();

            var resolver = new ServiceLoaderResolver(this, this.Container);
            return resolver.GetInstance(contractType, typeInitializationData);
        }
        
        protected override object ResolveByNamedInternal(Type contractType, string name, bool returnDefaultOnNotFound)
        {
            var typeInitializationDatum = this.typeCache[contractType].ToArray();
            var typeInitializationData = typeInitializationDatum.FirstOrDefault(x => x.Name == name);
            if (typeInitializationData == null)
            {
                if (returnDefaultOnNotFound)
                {
                    return null;
                }
                
                throw new KeyNotFoundException("The named item could not be found: '" + name + "'");
            }

            var resolver = new ServiceLoaderResolver(this, this.Container);
            return resolver.GetInstance(contractType, typeInitializationData);
        }
        
        protected override object[] ResolveAllInternal(Type contractType, bool returnDefaultOnNotFound)
        {
            var typeInitializationDatum = this.typeCache[contractType].ToArray();

            var resolver = new ServiceLoaderResolver(this, this.Container);
            return resolver.GetInstances(contractType, typeInitializationDatum);
        }

        private class ServiceLoaderResolver : ServiceLoaderInjectorBase, IServiceLoaderInjector
        {
            private readonly ServiceLoaderInjector serviceLoaderInjector;

            private readonly HasResolvedContainer container;
            
            public ServiceLoaderResolver(ServiceLoaderInjector serviceLoaderInjector, HasResolvedContainer container)
            {
                this.container = container;
                this.serviceLoaderInjector = serviceLoaderInjector;
            }

            public HasResolvedContainer Container
            {
                get { return this.container; }
            }
            
            public object GetInstance(Type contractType, TypeInitializationData typeInitializationData)
            {
                this.VerifyType(contractType);
                
                if (typeInitializationData.BindingType == ServiceLoaderBindingType.Singleton)
                {
                    if (typeInitializationData.HasResolved)
                    {
                        return typeInitializationData.Value;
                    }

                    object value = this.CreateType(typeInitializationData, contractType);
                    typeInitializationData.Value = value;
                    return value;
                }

                return this.CreateType(typeInitializationData, contractType);
            }

            public object[] GetInstances(Type contractType, IEnumerable<TypeInitializationData> typeInitializationDatum)
            {
                this.VerifyType(contractType);
                List<object> items = new List<object>();
                typeInitializationDatum.ForAll(typeInitializationData =>
                                               {
                                                   if (typeInitializationData.BindingType == ServiceLoaderBindingType.Singleton)
                                                   {
                                                       if (typeInitializationData.HasResolved)
                                                       {
                                                           items.Add(typeInitializationData.Value);
                                                       }
                                                       else
                                                       {
                                                           var value = this.CreateType(typeInitializationData, contractType);
                                                           typeInitializationData.Value = value;
                                                           items.Add(value);
                                                       }
                                                   }
                                                   else
                                                   {
                                                       items.Add(this.CreateType(typeInitializationData, contractType));
                                                   }
                                               });

                return items.ToArray();
            }

            protected override bool HasType(Type contractType)
            {
                return this.serviceLoaderInjector.typeCache.Contains(contractType);
            }
            
            protected override object ResolveInternal(Type contractType, bool returnDefaultOnNotFound)
            {
                if (!this.serviceLoaderInjector.typeCache.Contains(contractType))
                {
                    throw new KeyNotFoundException("Cannot resolve type: " + contractType.FullName);
                }
                
                var typeInitializationDatum = this.serviceLoaderInjector.typeCache[contractType].ToArray();
                var typeInitializationData = typeInitializationDatum.FirstOrDefault();
                return this.GetInstance(contractType, typeInitializationData);
            }
            
            protected override object ResolveByNamedInternal(Type contractType, string name, bool returnDefaultOnNotFound)
            {
                var typeInitializationDatum = this.serviceLoaderInjector.typeCache[contractType].ToArray();
                var typeInitializationData = typeInitializationDatum.FirstOrDefault(x => x.Name == name);
                if (typeInitializationData == null)
                {
                    if (returnDefaultOnNotFound)
                    {
                        return null;
                    }
                    
                    throw new KeyNotFoundException("The named item could not be found: '" + name + "'");
                }

                return this.GetInstance(contractType, typeInitializationData);
            }
            
            protected override object[] ResolveAllInternal(Type contractType, bool returnDefaultOnNotFound)
            {
                var typeInitializationDatum = this.serviceLoaderInjector.typeCache[contractType].ToArray();
                return this.GetInstances(contractType, typeInitializationDatum);
            }

            private object CreateType(TypeInitializationData typeInitializationData, Type contractType)
            {
                object value = typeInitializationData.CreationFunc(this);
                typeInitializationData.HasResolved = true;
                this.Container.ResolvedTypes.Add(contractType.GetHashCode());
                return value;
            }

            private void VerifyType(Type item)
            {
                var hashCode = item.GetHashCode();
                
                if (this.Container.RequestedTypes.Contains(hashCode) && !this.Container.ResolvedTypes.Contains(hashCode))
                {
                    throw new CircularDependencyException(item.Name);
                }
                
                this.Container.RequestedTypes.Add(hashCode);
            }
        }

        private class TypeInitializationData
        {
            private readonly Func<IServiceLoaderInjector, object> creationFunc;

            private readonly string name;

            private readonly ServiceLoaderBindingType bindingType;

            public TypeInitializationData(Func<IServiceLoaderInjector, object> creationFunc, string name, ServiceLoaderBindingType bindingType)
            {
                this.creationFunc = creationFunc;
                this.name = name;
                this.bindingType = bindingType;
            }

            public object Value { get; set; }

            public bool HasResolved { get; set; }

            public Func<IServiceLoaderInjector, object> CreationFunc
            {
                get
                {
                    return this.creationFunc;
                }
            }

            public string Name
            {
                get
                {
                    return this.name;
                }
            }

            public ServiceLoaderBindingType BindingType
            {
                get
                {
                    return this.bindingType;
                }
            }
        }
    }
}
