﻿namespace Base2art.ComponentModel
{
    using System;
    
    public interface IServiceLoaderBinding<T>
    {
        IServiceLoaderBinding<T> As(ServiceLoaderBindingType type);

        IServiceLoaderBinding<T> Named(string name);

        void To(Func<IServiceLoaderInjector, T> creationFunction);

        void To<TConcrete>(TConcrete concreteValue)
            where TConcrete : T;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "SjY")]
        void ToInstanceCreator<TConcrete>()
            where TConcrete : T;
    }
}
