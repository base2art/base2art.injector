﻿namespace Base2art.ComponentModel
{
    public static class ServiceLoader
    {
        public static IBindableAndSealableServiceLoaderInjector CreateLoader()
        {
            return new ServiceLoaderInjector();
        }
    }
}