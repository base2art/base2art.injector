﻿namespace Base2art.ComponentModel
{
    using System.Collections.Generic;
    
    public class HasResolvedContainer
    {
        private readonly HashSet<int> requestedTypes = new HashSet<int>();

        private readonly HashSet<int> resolvedTypes = new HashSet<int>();

        public HashSet<int> RequestedTypes
        {
            get
            {
                return this.requestedTypes;
            }
        }

        public HashSet<int> ResolvedTypes
        {
            get
            {
                return this.resolvedTypes;
            }
        }
    }
}
